package org.mygrpc.exception.handler;

public class GrpcException extends Exception {

	private String message;

	private String causedBy;

	private static final long serialVersionUID = 6941374454417783681L;

	public GrpcException(String message, String causedBy) {
		super();
		this.message = message;
		this.causedBy = causedBy;
	}

	@Override
	public String toString() {
		return "Cause-By" + this.causedBy + "Message " + message;
	}

	public GrpcException() {
		super();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCausedBy() {
		return causedBy;
	}

	public void setCausedBy(String causedBy) {
		this.causedBy = causedBy;
	}

}
