package org.mygrpc.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class BaseExceptionHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseExceptionHandler.class);

	@ExceptionHandler(GrpcException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorResponseDTO processGrpcServerExceptions(GrpcException eh) {
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Grpc Server Error ", eh.toString());
		}

		ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO();
		errorResponseDTO.setCause(eh.getCausedBy());
		errorResponseDTO.setMessage(eh.getMessage());
		return errorResponseDTO;
	}
}
