package org.mygrpc.exception.handler;

public class ErrorResponseDTO {
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCause() {
		return cause;
	}
	public void setCause(String cause) {
		this.cause = cause;
	}
	private String message;
	private String cause;
}
