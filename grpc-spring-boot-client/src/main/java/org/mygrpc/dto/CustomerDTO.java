package org.mygrpc.dto;

import java.io.Serializable;

public class CustomerDTO implements Serializable {
	private String emailAddress;
	private String customerName;

	public String getEmailAddress() {
		return emailAddress;
	}

	public CustomerDTO(String emailAddress, String customerName) {
		super();
		this.emailAddress = emailAddress;
		this.customerName = customerName;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

}
