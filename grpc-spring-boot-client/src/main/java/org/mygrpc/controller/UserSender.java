package org.mygrpc.controller;

import java.util.List;

import org.mygrpc.dto.CustomerDTO;
import org.mygrpc.exception.handler.GrpcException;
import org.mygrpc.grpc.services.CustomerDetailGrpc;
import org.mygrpc.grpc.services.CustomerDetailGrpc.CustomerDetailBlockingStub;
import org.mygrpc.grpc.services.CustomerRequest;
import org.mygrpc.grpc.services.CustomerResponse;
import org.mygrpc.grpc.services.Gender;
import org.mygrpc.grpc.services.UserDetail;
import org.mygrpc.grpc.services.UserServiceGrpc;
import org.mygrpc.grpc.services.UserServiceGrpc.UserServiceBlockingStub;
import org.mygrpc.grpc.services.user;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

@RestController
@RequestMapping("/grcp")
public class UserSender {

	@Autowired
	private DiscoveryClient discoveryClient;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserSender.class);

	@RequestMapping(method = RequestMethod.GET, value = "/senduser")
	public ResponseEntity<?> api() {
		user u = null;
		List<ServiceInstance> server = discoveryClient.getInstances("grpc-server");
		for (ServiceInstance serviceInstance : server) {

			String hostName = serviceInstance.getHost();
			int gRpcPort = Integer.parseInt(serviceInstance.getMetadata().get("grpc.port"));

			ManagedChannel channel = ManagedChannelBuilder.forAddress(hostName, gRpcPort).usePlaintext(true).build();
			UserServiceBlockingStub stub = UserServiceGrpc.newBlockingStub(channel);

			UserDetail user = UserDetail.newBuilder().setName("Thamira").setEmail("Thamira1005@gmail.com").setAge(24)
					.setGender(Gender.Male).setPassword("password").build();

			u = stub.createUser(user);
		}

		return ResponseEntity.ok("User " + u);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getCustomerDetails")
	public CustomerDTO getCustomerDetails() throws GrpcException {
		List<ServiceInstance> server = discoveryClient.getInstances("grpc-server");

		if (server.size() == 0) {
			LOGGER.error("grpc-server not registered ");
			throw new GrpcException("Server Not Found ", "Please check your grpc server");
		}
		CustomerResponse response = null;
		for (ServiceInstance serviceInstance : server) {
			String hostName = serviceInstance.getHost();
			int gRpcPort = Integer.parseInt(serviceInstance.getMetadata().get("grpc.port"));
			ManagedChannel channel = ManagedChannelBuilder.forAddress(hostName, gRpcPort).usePlaintext(true).build();

			CustomerDetailBlockingStub stub = CustomerDetailGrpc.newBlockingStub(channel);
			CustomerRequest request = CustomerRequest.newBuilder().setCustomerId("1").build();

			try {
				response = stub.getCustomerDetails(request);
			} catch (StatusRuntimeException s) {

			} catch (Exception e) {
				if (LOGGER.isErrorEnabled()) {
					LOGGER.error("Unable to get the get Data from Server ");
				}
			}
		}

		return new CustomerDTO(response.getEmailAdderess(), response.getCustomerName());
	}

}
