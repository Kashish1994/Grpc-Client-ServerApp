package org.mygrpc.cervice.grcp;

import org.lognet.springboot.grpc.GRpcService;
import org.mygrpc.cervice.customer.domain.Customer;
import org.mygrpc.cervice.customer.domain.CustomerRepository;
import org.mygrpc.grpc.services.CustomerDetailGrpc;
import org.mygrpc.grpc.services.CustomerRequest;
import org.mygrpc.grpc.services.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;

import io.grpc.stub.StreamObserver;

@GRpcService
public class CustomerServiceImpl extends CustomerDetailGrpc.CustomerDetailImplBase {
	@Autowired
	private CustomerRepository customerRepo;

	@Override
	public void getCustomerDetails(CustomerRequest request, StreamObserver<CustomerResponse> responseObserver) {
		Customer customer = customerRepo.findOne(Long.valueOf(request.getCustomerId()));

		CustomerResponse.Builder responseBuilder = CustomerResponse.newBuilder();
		responseBuilder.setCustomerName(customer.getCustomerName());
		CustomerResponse response = responseBuilder.setEmailAdderess(customer.getEmailAddress()).build();
		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}
}
